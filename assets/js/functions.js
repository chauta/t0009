/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

/*===================================================
slice
===================================================*/
$(document).ready(function() {
  $('.c-slide1').slick({
    prevArrow:false,
    nextArrow: false,
    slidesToShow:1,
    autoplay: true,
    autoplaySpeed:4000,
    dots:true,
  });
});

/*===================================================
fixed logo
===================================================*/
$(document).ready(function() {
  var scrolltop1 = $("#fixed2");
  var scrolltop2 = $("#fixed1");
  var scrolltopflow= $('.page-flow .c-fixed1');

  $(window).scroll(function() {
    var topPos = $(this).scrollTop();
    if(topPos > 6400){
      $(scrolltop1).css("position","absolute");
    }
    else{
      $(scrolltop1).css("position","fixed");
    }
    if(topPos > 6950){
      $(scrolltop2).css("position","absolute");
    }
    else{
      $(scrolltop2).css("position","fixed");
    }
    if(topPos > 3200){
      $(scrolltopflow).css("position","absolute");
    }
    else{
      $(scrolltopflow).css("position","fixed");
    }
  });
});

/*===================================================
inview footer image
===================================================*/
$(function(){
  $('.c-inview').css( 'opacity',0);

  $('.c-inview').on('inview', function(event, isInview){
    if (isInview){
      $(this).stop().animate({opacity: 1},1500);
      $(this).addClass('animated fadeInDown');
    }
    else{
      $(this).stop().animate({opacity: 0},1500);
       $(this).removeClass('animated fadeInDown');
    }
  });
});
/*===================================================
menu active
===================================================*/
$(function(){
  var  u= window.location.href;
  if(/flow/.test(u)){
    $('.c-menuflow a').addClass('u-bondiblue');
  }
  if(/Q&A/.test(u)){
    $('.c-menuQA a').addClass('u-bondiblue');
    $(window).scroll(function() {
      var topPos = $(this).scrollTop();
      if(topPos > 1500){
        $('.c-fixed1').css("position","absolute");
      }
      else{
        $('.c-fixed1').css("position","fixed");
      }
    });
  }
});

/*===================================================
accordion QA
===================================================*/
$(function(){
  $(".p-QAcordion .c-QA:first-child .c-QA__content").css("display","block");
  $(".p-QAcordion .c-QA:first-child").addClass('active');
  $(".c-QA__title").click(function(){
    $a=$(this).next("div");
    if($a.is(':hidden')==true){
      $(".p-QAcordion .c-QA__content").slideUp("slow");
      $a.slideDown();
      $(this).parent().addClass("active");
    }else{
      $a.slideUp();
      $(this).parent().removeClass("active");
    }
  });

  $(".c-Qa__title:eq(1)").trigger('click');
});