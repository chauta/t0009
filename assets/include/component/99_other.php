<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2"></div>
<div class="c-menu">
	<div class="c-logo">
		<p>一級建築士事務所<span class="u-bondiblue">シープ</span></p>
		<img src="assets/image/common/common_07.png" alt="">
	</div>
	<div class="c-menu__ad">
		日々をたのしむ住まい。
	</div>
	<ul class="c-menu__ul">
	    <li>
	    	<a href="">
	    		Q&A
	    	</a>
		</li>
	   <li>
	    	<a href="">
	    		ブログ
	    	</a>
		</li>
		<li>
	    	<a href="">
	    		住まいづくりのすすめ方
	    	</a>
		</li>
		<li>
	    	<a href="">
	    		ギャラリー
	    	</a>
		</li>
		<li>
	    	<a href="">
	    		デザインコンセプト
	    	</a>
		</li>
		<li>
	    	<a href="">
	    		シープについて
	    	</a>
		</li>
	</ul>
	<div class="c-menu__bot">
		<p>
			<a href="">
				サイトマップ
			</a>
		</p>
		<p>
			<a href="">
				プライバシーポリシー
			</a>
		</p>
	</div>
	<div class="c-menu__abs">
		<div class="c-menu__home">
			<a href="">HOME</a>
			<a href="">へ戻る</a>
		</div>
	</div>
</div>

	<div class="c-imgcontact c-imgcontact--2">
		<span class="c-imgcontact__title">
			Contact
		</span>
		<p>
			お問い合わせ
		</p>
	</div>