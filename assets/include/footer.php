<footer>
	<div class="c-footsitemap">
		<nav class="c-breadcrumb1">
		    <ul>
		        <li><a href="#">ホーム</a></li>
		    	<li><a href="#">ページトップへ</a></li>
		    </ul>
		</nav>
		<div class="c-footsitemap__inner">
			<div class="c-footsitemap__col">
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							About
						</p>
						<span>
							シープについて
						</span>
					</div>
					<ul>
					    <li>
					    	<a href="">
					    		設計事務所と創りましょう
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		事務所概要
					    	</a>
					    </li> <li>
					    	<a href="">
					    		プロフィール
					    	</a>
					    </li> <li>
					    	<a href="">
					    		アクセスマップ
					    	</a>
					    </li>
					</ul>
				</div>
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Gallery
						</p>
						<span>
							ギャラリー
						</span>
					</div>
				</div>
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Flow
						</p>
						<span>
							住まいづくりのすすめ方
						</span>
					</div>
				</div>
			</div>
			<div class="c-footsitemap__col2">
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Desing
						</p>
						<span>
							デザインコンセプト
						</span>
					</div>
					<ul>
					    <li>
					    	<a href="">
					    		光
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		風
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		省エネ
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		安心・安全
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		広がり・つながり
					    	</a>
					    </li>
					    <li>
					    	<a href="">
					    		シンプル
					    	</a>
					    </li>
					</ul>
				</div>
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Contact
						</p>
						<span>
							お問い合わせ
						</span>
					</div>
				</div>
			</div>
			<div class="c-footsitemap__col3">
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Q&A
						</p>
						<span>
							質問と解答
						</span>
					</div>
				</div>
				<div class="c-footsitemap__block">
					<div class="c-footsitemap__title">
						<p>
							Blog
						</p>
						<span>
							ブログ
						</span>
					</div>
				</div>
				<div class="c-footsitemap__note">
					<ul>
					    <li>
					    	<a href="">
						    	プライバシーポリシー
						    </a>
						</li>
					    <li>
					    	<a href="">
						    	サイトマップ
						    </a>
						</li>
					</ul>
				</div>			
			</div>
			<div class="c-footsitemap__col4">
				<div class="c-footsitemap__block2">
					<div class="c-footsitemap__socy">
						<img src="assets/image/top/top_15.png" alt="">
						<p>
							公式Facebookはこちら
						</p>
					</div>
				</div>
				<div class="c-footsitemap__block2">
					<div class="c-footsitemap__socy">
						<img src="assets/image/top/top_16.png" alt="">
						<p>
							ひつじ的住まいズム！
						</p>
					</div>
				</div>
				<div class="c-footsitemap__block2">
					<div class="c-footsitemap__socy">
						<img src="assets/image/top/top_16.png" alt="">
						<p>
							ヒツジ的フォトライフ！
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-footcontact">
		<div class="l-main">
			<div class="c-footcontact__fle">
				<div class="c-footcontact__left">
					<p>
						一級建築士事務所<span class="u-bondiblue">シープ</span>
					</p>
					<img src="assets/image/common/common_07-2.png" alt="">
				</div>
				<div class="c-footcontact__right">
					<p>
						一級建築士事務所登録　静岡県知事登録（◯-00）第00000号<br>
						〒400-0000静岡県浜松市中区三組町28-76<br>
						TEL：053-000-0000　FAX：053-000-0000
					</p>
					<div class="c-footcontact__abs">
						<img src="assets/image/common/common_02.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="l-container">
		<div class="c-footerimage c-inview">
			<img src="assets/image/common/common_03.png" alt="">
			<p class="c-footerimage__abs">
				Copyright © 2015 SHEaP inc. All Rights Reserved.
			</p>
		</div>
	</div>
</footer>





<script src="assets/js/slick/slick.js"></script>
<script src="assets/js/jquery.inview.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>