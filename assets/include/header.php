<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link  href="/assets/js/slick/slick.css" rel="stylesheet">
<link  href="/assets/js/animate.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

	<header id="header" class="">
		<div id="fixed1" class="c-fixed1">
			<div class="l-main c-fixed1__mor">
				<div class="c-fixed1__menu">
					<div class="c-menu">
						<div class="c-logo">
							<p>一級建築士事務所<span class="u-bondiblue">シープ</span></p>
							<img src="assets/image/common/common_07.png" alt="">
						</div>
						<div class="c-menu__ad">
							日々をたのしむ住まい。
						</div>
						<ul class="c-menu__ul">
						    <li class="c-menuQA">
						    	<a href="/Q&A.php">
						    		Q&A
						    	</a>
							</li>
						   <li>
						    	<a href="">
						    		ブログ
						    	</a>
							</li>
							<li class="c-menuflow">
						    	<a href="/flow.php">
						    		住まいづくりのすすめ方
						    	</a>
							</li>
							<li>
						    	<a href="">
						    		ギャラリー
						    	</a>
							</li>
							<li>
						    	<a href="">
						    		デザインコンセプト
						    	</a>
							</li>
							<li>
						    	<a href="">
						    		シープについて
						    	</a>
							</li>
						</ul>
						<div class="c-menu__bot">
							<p>
								<a href="">
									サイトマップ
								</a>
							</p>
							<p>
								<a href="">
									プライバシーポリシー
								</a>
							</p>
						</div>
						<div class="c-menu__abs">
							<div class="c-menu__home">
								<a href="top.php">HOME</a>
								<a href="">へ戻る</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="fixed2" class="c-fixed1 c-fixed1--con2">
			<div class="l-main c-fixed1__mor">
				<div class="c-fixed1__icon">
					<div class="c-imgcontact c-imgcontact--2">
						<a href="">
							<span class="c-imgcontact__title">
							Contact
							</span>
							<b>
								お問い合わせ
							</b>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>