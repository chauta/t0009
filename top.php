<?php $id="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>



<div class="l-container">
	<div class="p-pagetop">
		<div class="p-top1">
			<div class="l-main">
				<div class="c-top1">
					<div class="l-content">
						<div class="c-title1">
							<p>
								シープは静岡、愛知、岐阜、三重、を中心とした一級建築士事務所です。
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="c-slide">
				<div class="c-slide1">
					<div>
						<img src="assets/image/top/top_01.jpg" alt="">
					</div>
					<div>
						<img src="assets/image/top/top_01.jpg" alt="">
					</div>
					<div>
						<img src="assets/image/top/top_01.jpg" alt="">
					</div>
					<div>
						<img src="assets/image/top/top_01.jpg" alt="">
					</div>
					<div>
						<img src="assets/image/top/top_01.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="l-main">
			<div class="p-top2">
				<div class="l-content">
					<div class="c-top2">
						<div class="c-title2">
							<p>
								スタイリッシュで<br>
								人と地球に優しい住まいづくり
							</p>
						</div>
						<div class="c-top2__txt">
								<p>
									 豊かな住まいとは、ただ広く、豪華な素材を使うことではありません <br>
									 本当の意味での「豊かな住まい」とは 住まう人に合っていることなのではないでしょうか<br>
									 じっくりと話し、ライフスタイルにフィットしたものを見つけ出していく<br>
									 一級建築士事務所 SHEaPは、そんな設計を心がけています 
								</p>
						</div>
					</div>
					<div class="p-top2con2">
						<div class="c-top2p2">
							<div class="c-top2p2__abs">
								<img src="assets/image//top/top_02.png" alt="">
							</div>
							<div class="p-top2about">
								<div class="c-top2fx">
									<div class="c-top2fx__con">
										<div class="c-top2fx__circle">
											<div class="c-circle">
												<div class="c-circle2">
													<p class="u-bondiblue">
														SHEaPについて
													</p>
													<p>
														Ab<span class="u-bondiblue">o</span>ut
													</p>
												</div>
												<a class="c-sheep">
													<span class="u-bondiblue">
														もっと見る
													</span>
												</a>
											</div>
										</div>
									</div>
									<div class="c-top2fx__con">
										<div class="c-title4">
											<p>
												こんにちは、<br>
												一級建築士事務所SHEaPの<br>
												戸塚治夫です。
											</p>
										</div>
										<div class="c-top2fx__txt">
											<p>
												住まいづくりをする方が一番意識するのは、間取りや材料、
												<br>デザイン、そして、コストとのバランスです。それを総合
												<br>的に提案するのが設計者の役割だと考えています。
											</p>
											<p>
												せっかくの住まいづくりですから、より住まう自分達にあったものを追求して欲しいと思います。
											</p>
											<p>
												住まう方にフィットする住まいを提供する為に、お客様とのコミュニケーションを大切にして設計したいと思っています。 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="p-top2con3">
						<div class="c-top2fx c-top2fx--ve2">
							<div class="c-top2fx__con">
								<div class="c-title3">
									<p>
										Des<span class="u-pink">i</span>ng 
									</p>
									<span class="c-subtitle u-pink">6つの設計コンセプト</span>
								</div>
								<div class="c-title4">
									<p>
										SHEaPでは、<br>
										6つの設計コンセプトを<br>
										大切にしています。
									</p>
								</div>
								<div class="c-top2fx__txt">
									<p>
										一級建築士事務所SHEaPでは、<br>
										「光」「風」「省エネ」「安心・安全」「広がり・つながり」<br>「シンプル」といった6つの設計コンセプトを大切<br>に考えています。

									</p>
								</div>
							</div>
							<div class="c-top2fx__con">
								<div class="c-top2fx__circle">
									<div class="c-circle c-circle--cl2">
										<div class="c-circle2 c-circle2--cl2">
											<p class="u-pink">
												6つの設計コンセプト
											</p>
											<p>
												Des<span class="u-pink">i</span>ng
											</p>
										</div>
										<a class="c-sheep c-sheep--cl2">
											<span class="u-pink">
												もっと見る
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="p-top3">
				<div class="l-content">
					<div class="c-top3">
						<div class="c-title3">
							<p>
								G<span class="u-bondiblue">a</span>llery
							</p>
							<span class="c-subtitle u-bondiblue">設計事例と提案事例</span>
						</div>
						<div class="c-title4">
							<p>
								SHEaPのお仕事の一部をご覧ください。
							</p>
						</div>
						<div class="c-list1">
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<img src="assets/image/top/top_05.png" alt="">
							</div>
							<div class="c-list1__img">
								<a class="c-sheep">
									<span class="u-bondiblue">
										もっと見る
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="p-top4">
				<div class="c-top4">
					<div class="l-content">
						<div class="c-title3">
							<p>
								Sched<span class="u-pink">u</span>le
							</p>
							<span class="c-subtitle u-pink">住まいづくりのすすめ方</span>
						</div>
						<div class="c-title4">
							<p>
								住まいづくりの流れをご紹介しています。
							</p>
						</div>
						<div class="c-top4__txt1">
							<p>
								この限りではありませんので、ご要望に合わせてご相談ください。
							</p>
						</div>
						<div class="p-top4__in">
							<div class="c-imgtext">
								<div class="c-imgtext__img">
									<img src="assets/image/top/top_06.png" alt="">
								</div>
								<div class="c-imgtext__text">
									<div class="c-title5">
										<p>
											<span class="u-pink">1.</span>聞く
										</p>
										<span class="u-pink c-title5__subtitle">
											基本計画段階 1.5〜2ヶ月
										</span>
									</div>
									<div class="c-textbor">
										<p class="u-pink">
											お引き合い・打ち合せ
										</p>
										<p>
											・現在の悩みと「こうなりたい」のイメージ<br>
											・敷地の調査<br>
											・法規チェック
										</p>
									</div>
									<div class="c-textbor c-textbor--af2">
										<p class="u-pink">
											プレゼンテーション
										</p>
										<p>
											・基本計画図（配置図・平面図・立面図）<br>
											・イメージスケッチ等<br>
											・設計業務 概算のお見積もり<br>
										</p>
									</div>
								</div>
							</div>
							<div class="c-imgtext">
								<div class="c-imgtext__img">
									<img src="assets/image/top/top_07.png" alt="">
								</div>
								<div class="c-imgtext__text">
									<div class="c-title5">
										<p>
											<span class="u-pink">2.</span>考える
										</p>
										<span class="u-pink c-title5__subtitle">
											設計段階 3〜6ヶ月
										</span>
									</div>
									<div class="c-textbor">
										<p class="u-pink">
											お引き合い・打ち合せ
										</p>
										<p>
											・現在の悩みと「こうなりたい」のイメージ<br>
											・敷地の調査<br>
											・法規チェック
										</p>
									</div>
									<div class="c-textbor c-textbor--af2">
										<p class="u-pink">
											プレゼンテーション
										</p>
										<p>
											・基本計画図（配置図・平面図・立面図）<br>
											・イメージスケッチ等<br>
											・設計業務 概算のお見積もり<br>
										</p>
									</div>
								</div>
							</div>
							<div class="c-imgtext">
								<div class="c-imgtext__img">
									<img src="assets/image/top/top_08.png" alt="">
								</div>
								<div class="c-imgtext__text">
									<div class="c-title5">
										<p>
											<span class="u-pink">3.</span>聞く
										</p>
										<span class="u-pink c-title5__subtitle">
											基本計画段階 1.5〜2ヶ月
										</span>
									</div>
									<div class="c-textbor">
										<p class="u-pink">
											お引き合い・打ち合せ
										</p>
										<p>
											・現在の悩みと「こうなりたい」のイメージ<br>
											・敷地の調査<br>
											・法規チェック
										</p>
									</div>
									<div class="c-textbor c-textbor--af2">
										<p class="u-pink">
											プレゼンテーション
										</p>
										<p>
											・基本計画図（配置図・平面図・立面図）<br>
											・イメージスケッチ等<br>
											・設計業務 概算のお見積もり<br>
										</p>
									</div>
								</div>
							</div>
							<div class="c-imgtext c-imgtext--bet">
								<div class="c-imgtext__img">
									<img src="assets/image/top/top_09.png" alt="">
								</div>
								<div class="c-imgtext__text">
									<div class="c-title5">
										<p>
											<span class="u-pink">4.</span>住まう
										</p>
										<span class="u-pink c-title5__subtitle">
											お引渡し
										</span>
									</div>
									<div class="c-textbor">
										<p class="u-pink">
											お引き合い・打ち合せ
										</p>
										<p>
											・現在の悩みと「こうなりたい」のイメージ<br>
											・敷地の調査<br>
											・法規チェック
										</p>
									</div>
									<div class="c-textbor c-textbor--af2">
										<p class="u-pink">
											プレゼンテーション
										</p>
										<p>
											・基本計画図（配置図・平面図・立面図）<br>
											・イメージスケッチ等<br>
											・設計業務 概算のお見積もり<br>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-top4__sheep">
							<a class="c-sheep c-sheep--cl2">
								<span class="u-pink">
									詳しく見る
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="p-top5">
				<div class="c-top5">
					<div class="c-blog">
						<div class="c-blog__abs">
							<a class="c-sheep">
								<span class="u-bondiblue">
									もっと見る
								</span>
							</a>
						</div>
						<div class="c-blog__title">
							<p>
								BLOG
							</p>
							<span>
								ヒツジ的住まいズム！
							</span>
						</div>
						<div class="c-blog__con">
							<h6>
								タイトル
							</h6>
							<p>
								&emsp;記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。
							</p>
							<p>
								&emsp;記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。<br>
								<span>
									記事内容がここに表示されます。記事内容がここに表示されます。
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="p-top6">
				<div class="l-content">
					<div class="c-top6">
						<div class="p-top6in1">
							<div class="c-title3">
								<p>Q<span class="u-pink">&</span>A</p>
								<span class="c-subtitle u-pink">よく寄せられる質問</span>
							</div>
							<div class="c-list2">
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q1.
										</h6>
										<p>
											設計事務所って<br>
											高いんじゃないの？
										</p>
									</div>
								</div>
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q2.
										</h6>
										<p>
											工事は請け負わないの？
										</p>
									</div>
								</div>
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q3.
										</h6>
										<p>
											小さな会社に依頼しても<br>
											安心できますか？
										</p>
									</div>
								</div>
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q4.
										</h6>
										<p>
											設計事務所って<br>
											高いんじゃないの？
										</p>
									</div>
								</div>
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q5.
										</h6>
										<p>
											工事は請け負わないの？
										</p>
									</div>
								</div>
								<div class="c-list2__qa">
									<div class="c-list2__img">
										<img src="assets/image/top/top_10.png" alt="">
									</div>
									<div class="c-list2__info">
										<h6>
											Q6.
										</h6>
										<p>
											小さな会社に依頼しても<br>
											安心できますか？
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-top6__abs">
							<a class="c-sheep c-sheep--cl2">
								<span class="u-pink">
									詳しく見る
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="p-contact">
			<div class="l-main">
				<div class="l-content">
					<div class="c-title3">
						<p>Contact</p>
						<span class="c-subtitle u-pink">お問い合わせ</span>
					</div>
				</div>
			</div>
			<div class="c-title4">
				<p>
					住まいづくり・建築計画に係る相談を承っております。<br>
					相談は無料です。気軽にお声かけください。 
				</p>
			</div>
			<div class="l-main">
				<div class="l-content">
					<div class="c-contact">
						<div class="c-form1">
							<form>
								<table>
										<tr>
											<th>
												お名前
												<span class="u-pink">【必須】</span>
											</th>
											<td>
												<input type="text">
											</td>
										</tr>
										<tr>
											<th>
												Email
												<span class="u-pink">【必須】</span>
											</th>
											<td>
												<input type="text">
											</td>
										</tr>
										<tr>
											<th>お問い合わせ種別</th>
											<td class="c-radiobtn">
												<label class="checkin">新築
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
												<label class="checkin">リフォーム
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
												<label class="checkin">アパート・マンションオーナー様
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
												<label class="checkin">相談会申込み
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
												<label class="checkin">その他
													<input type="checkbox">
													<span class="checkmark"></span>
												</label>
											</td>
										</tr>
										<tr>
											<th>郵便番号</th>
											<td>
												<input type="text">
											</td>
										</tr>
										<tr>
											<th>ご住所</th>
											<td>
												<input type="text">
											</td>
										</tr>
										<tr>
											<th>お電話番号</th>
											<td>
												<input type="text">
											</td>
										</tr>
										<tr>
											<th>
												お問い合わせ内容
												<span class="u-pink">【必須】</span>
											</th>
											<td >
												 <textarea></textarea>
											</td>
										</tr>
								</table>
								<div class="c-formbtn">
									<button type="reset">リセット</button>
									<button type="submit">確認画面へ</button>
								</div>
							</form>
						</div>
						<div class="c-contact__con">
							<div class="c-contact__txt">
								<p>
									お問い合わせ・ご相談はお気軽に

								</p>
								<p class="c-sdt">
									090-5870-0000
								</p>
								<span>受付時間　10：00〜20：00　◯曜定休</span>
							</div>
							<div class="c-contact__img">
								<div class="c-imgcontact">
									<a href="">
										<span class="c-imgcontact__title">
											Contact
										</span>
										<b>
											お問い合わせ<br>
											フォーム
										</b>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>