<?php $id="flow";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>


<div class="l-container">
	<div class="p-pageflow">
		<div class="l-main">
			<div class="l-content">
				<div class="c-breadcrumb2">
					<ul>
					    <li>
					    	<a href="/top.php">
					    	トップページ
					    	</a><span>＞</span>
						</li>
						 <li>
					    	<a href="">
					    	住まいづくりのすすめ方
					    	</a>
						</li>
					</ul>
				</div>
				<div class="p-flow1">
					<div class="c-title3">
						<p>
							F<span class="u-bondiblue">l</span>ow 
						</p>
						<span class="c-subtitle u-bondiblue">
							住まいづくりのすすめ方
						</span>
					</div>
					<div class="c-flow1">
						<div class="c-flow1__img">
							<img src="assets/image/flow/flow_main.png" alt="">
						</div>
						<div class="c-flow1__txt">
							<p>
								住宅にせよ、業務用の建物にせよ、初めての方が大半だと思います。<br>
								「何から始めて良いか？」わからないのは当然です。<br>
								そういった時に、最初に相談する窓口が我々「建築設計事務所」です。<br>
								一級建築士事務所SHEaPでは、建物のことだけでなく、<br>
								住まいづくりにおける資金計画や土地のこと、<br>
								事業用の土地や運営計画まで、<br>
								建築に関わる初期段階の相談を承っております。<br>
								気軽にお声かけください。


							</p>
							<p>
								 下記に、住宅の場合を例に大まかなスケジュールを表示しましたので、ごらんください。 
							</p>
						</div>
					</div>
				</div>
				<div class="p-flow2">
					<div class="p-flow2out1">
						<div class="c-flow2">
							<div class="c-flow2__title">
								<div class="c-imgttl">
									<div>
										<img src="assets/image/flow/flow_01.jpg" alt="">
									</div>
									<div>
										<img src="assets/image/flow/tl1.png" alt="">
									</div>
								</div>
							</div>
							<div class="c-imgtext2">
								<div class="c-imgtext2__img">
									<img src="assets/image/flow/img1.png" alt="">
								</div>
								<div class="c-imgtext2__text">
									<p>
										<span>まずは、お気軽にご連絡ください。</span><span>実際の暮らしを体感していただくことで、間取り、光や風の入り方、生活動線などの空間イメージや設備機器、建材などの素材のイメージの参考にしていただきやすいかと思います。</span><br>
										自分らしい住まいづくりのスタートです。
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="p-flow2out2">
						<div class="c-flow2">
							<div class="c-flow2__title">
								<div class="c-imgttl c-imgttl--ml2">
									<div>
										<img src="assets/image/flow/flow_02.jpg" alt="">
									</div>
									<div>
										<img src="assets/image/flow/tl2.png" alt="">
									</div>
								</div>
							</div>
							<div class="c-imgtext2 c-imgtext2--reverse">
								<div class="c-imgtext2__img">
									<img src="assets/image/flow/img2.png" alt="">
								</div>
								<div class="c-imgtext2__text c-imgtext2__text--2">
									<p>
										家族の顔が見える家、趣味を生かした家、ペットと暮らしやすい家、シンプルなデザインの家、窓の大きい家など、世界に一つしかないお客様の建てたい家のイメージや想いをたくさんお聞かせください。<br>

									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="p-flow2out3">
						<div class="c-flow2">
							<div class="c-flow2__title">
								<div class="c-imgttl">
									<div>
										<img src="assets/image/flow/flow_03.jpg" alt="">
									</div>
									<div>
										<img src="assets/image/flow/tl3.png" alt="">
									</div>
								</div>
							</div>
							<div class="c-imgtext2">
								<div class="c-imgtext2__img">
									<img src="assets/image/flow/img3.png" alt="">
								</div>
								<div class="c-imgtext2__text">
									<p>
										できあたってきたイメージをもと耐震・耐熱・防犯などの機能性も考慮しながら、アウトラインからでティールまで<span>カタチを作っていきます。</span>いよいよ、お客様個々の住まいが図面となり、平面上に絵となって浮かび上がります。
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="p-flow2out4">
						<div class="c-flow2">
							<div class="c-flow2__title">
								<div class="c-imgttl c-imgttl--ml2">
									<div>
										<img src="assets/image/flow/flow_04.jpg" alt="">
									</div>
									<div>
										<img src="assets/image/flow/tl4.png" alt="">
									</div>
								</div>
							</div>
							<div class="c-imgtext2 c-imgtext2--reverse">
								<div class="c-imgtext2__img">
									<img src="assets/image/flow/img4.png" alt="">
								</div>
								<div class="c-imgtext2__text">
									<p>
										近隣へと挨拶と鎮魂祭から始まります。<br>
										<span>家の大きさにもよりますが、約3か月から5か</span><br>月間、各業種のエキスパートが住まいづくりに関<br>わります。各工程の記録、検査を経てオンリーワ<br>ンの住まいが建ち上がります。


									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="p-flow2out5">
						<div class="c-flow2">
							<div class="c-flow2__title">
								<div class="c-imgttl c-imgttl--ml2">
									<div>
										<img src="assets/image/flow/flow_05.jpg" alt="">
									</div>
									<div>
										<img src="assets/image/flow/tl5.png" alt="">
									</div>
								</div>
							</div>
							<div class="c-imgtext2 c-imgtext2--wid2">
								<div class="c-imgtext2__img">
									<img src="assets/image/flow/img5.png" alt="">
								</div>
								<div class="c-imgtext2__text">
									<p>
										完成後、竣工検査などを経て、晴れてお客様へとお引き渡しいたします。ついに、お客様だけの住まいの完成です。住み始めてからの不具合や、修繕、増築、改築なども、もちろんお気軽にご相談ください。<br>
										末長いお付き合いをお願いいたします。
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="p-flow3">
					<div class="c-flow3">
						<div class="c-flow3__imgttl">
							<img src="assets/image/flow/txt1.png" alt="">
						</div>
						<div class="c-flow3__text">
							<p>
								設計事務所の住まいづくりは、決して高くはありません。
							</p>
							<p>
								よく、「設計事務所に依頼すると、設計費が余分にかかるんじゃないの？」と言われますが、決してそうではありません。ハウスメーカー、工務店、大工さんに依頼する場合も、設計費という項目がない会社もありますが、必ず設計費はかかっているのです。
							</p>
							<p>
								設計事務所から工務店に依頼する場合、当然、設計費を抜いた金額で依頼する為、同じ建物を建てるならば、設計施工の会社に依頼する場合と比べて高くなるということはありません。
							</p>
							<p>
								もちろん、条件が難しかったり、特別なこだわりがある方の場合は、工事も含めそれなり費用になりますが、そういった場合が設計事務所としての腕の見せ所だと思っていますし、得意としてます。
							</p>
						</div>
					</div>
					<div class="c-flow3">
						<div class="c-flow3__imgttl">
							<img src="assets/image/flow/txt2.png" alt="">
						</div>
						<div class="c-flow3__text">
							<p>
								計画建物の大きさや、敷地、申請、用途などの条件によってさまざまですが、<br>
								おおよそ、建築工事費の6％～10％です。<br>
								建物が大きいほど、スケールメリットがでて、設計費も安くなります。
							</p>
							<p>
								<span>30～40坪で、基本計画、実施設計、設計監理、外構計画、インテリアコーディネートなど、全て含んで、工事</span>金額の8％くらいが目安となります。
							</p>
						</div>
					</div>
				</div>
				<div class="p-flowcontact">
					<div class="c-contact">
						<div class="c-contact__con">
							<div class="c-contact__txt">
								<p>
									お問い合わせ・ご相談はお気軽に

								</p>
								<p class="c-sdt">
									090-5870-0000
								</p>
								<span>受付時間　10：00〜20：00　◯曜定休</span>
							</div>
							<div class="c-contact__img">
								<div class="c-imgcontact">
									<a href="">
										<span class="c-imgcontact__title">
											Contact
										</span>
										<b>
											お問い合わせ<br>
											フォーム
										</b>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>